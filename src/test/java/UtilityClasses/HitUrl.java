package UtilityClasses;

import java.util.LinkedHashMap;
import java.util.Map;

import Model.ResponseAndToken;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class HitUrl {

	public static ResponseAndToken save_card(int form, String url, String key, String command, String var1, String var2,
			String var3, String var4, String var5, String var6, int var7, int var8) {
		Response response = null;
		String hash = Hash.get_SHA_512_SecurePassword(key + "|" + command + "|" + var1 + "|" + "1b1b0");
		RestAssured.baseURI = url;
	
		response = RestAssured.given().urlEncodingEnabled(true).param("form", form).param("key", key)
				.param("command", command).param("hash", hash).param("var1", var1).param("var2", var2)
				.param("var3", var3).param("var4", var4).param("var5", var5).param("var6", "4137894711755904")
				.param("var7", var7).param("var8", "2021").post().then().statusCode(200).extract().response();

		ResponseAndToken rt = new ResponseAndToken();
		rt.setResponse(response);
		JsonPath json = response.jsonPath();
		String token = json.get("cardToken");
		rt.setToken(token);
		
		return rt;
	}

	public static ResponseAndToken get_card(int form, String url, String key, String command, String var1, String var2,
			String var3, String var4, String var5, String var6, int var7, int var8) {

		ResponseAndToken rt = new ResponseAndToken();
		
		String hash = Hash.get_SHA_512_SecurePassword(key + "|" + command + "|" + var1 + "|" + "1b1b0");
		RestAssured.baseURI = url;
		Response response = RestAssured.given().urlEncodingEnabled(true).param("form", form).param("key", key)
				.param("command", command).param("hash", hash).param("var1", var1).post().then().statusCode(200)
				.extract().response();

		JsonPath json = response.jsonPath();
		String token;
		String msg = json.get("msg");

		if (msg.equals("Card not found.")) {
			token = "No token";
		} else {
			Map<String, Map<String, String>> val = new LinkedHashMap<String, Map<String, String>>();
			val = json.get("user_cards");
			Map.Entry<String, Map<String, String>> entry = val.entrySet().iterator().next();
			token = entry.getKey();
		}
		// System.out.println(token);
		rt.setResponse(response);
		rt.setToken(token);
		return rt;
	}

	public static ResponseAndToken delete_card(int form, String url, String key, String command, String var1,
			String var2, String var3, String var4, String var5, String var6, int var7, int var8) {
		ResponseAndToken rt = new ResponseAndToken();
		Response response = null;
		
		String hash = Hash.get_SHA_512_SecurePassword(key + "|" + command + "|" + var1 + "|" + "1b1b0");
		RestAssured.baseURI = url;

		response = RestAssured.given().urlEncodingEnabled(true).param("form", form).param("key", key)
				.param("command", command).param("hash", hash).param("var1", var1).param("var2", var2).post().then()
				.statusCode(200).extract().response();
		rt.setResponse(response);
		rt.setToken("");
		return rt;
	}


///////////////////////////////////////////
	public static ResponseAndToken hitUrl(int form, String url, String key, String command, String var1, String var2,
			String var3, String var4, String var5, String var6, int var7, int var8) {
		ResponseAndToken rt = new ResponseAndToken();

		if (command.equals("save_user_card")) {
			rt = save_card(form, url, key, command, var1, var2, var3, var4, var5, var6, var7, var8);
		} else if (command.equalsIgnoreCase("get_user_cards")) {
			rt = get_card(form, url, key, command, var1, var2, var3, var4, var5, var6, var7, var8);
		} else if (command.equalsIgnoreCase("delete_user_card")) {
			rt = get_card(form, url, key, "get_user_cards", var1, var2, var3, var4, var5, var6, var7, var8);
			String token = rt.getToken();
			if(token.equalsIgnoreCase("No token")) {
				return rt;
			}
			rt = delete_card(form, url, key, command, var1, token, var3, var4, var5, var6, var7, var8);
		}
		return rt;
	}

}
