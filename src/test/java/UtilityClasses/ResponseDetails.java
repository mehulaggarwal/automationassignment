package UtilityClasses;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ResponseDetails {
	

	public static String getMessage(Response response) {
		JsonPath json=response.jsonPath();
		String message = json.get("msg");
		return message;
	}


	public static String getContentType(Response response) {
		String[] contentType=response.getHeaders().getValue("content-Type").split(";");
		return contentType[0];
	}
	

}
