package UtilityClasses;


import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;



public class ReadFromExcel {
	
	public int getRowStart() {
		int rowStart=0;
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("Mehul.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			rowStart = sheet.getFirstRowNum();
			file.close();
		}
		catch(Exception e) {
			System.out.println("Error in getting row Start");
		}
		return rowStart;
	}
	
	public int getRowEnd() {
		int rowStart=0;
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("Mehul.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			rowStart = sheet.getLastRowNum();
			file.close();
		}
		catch(Exception e) {
			System.out.println("Error in getting row End");
		}
		return rowStart;
	}
	
	public String[] getUrl() {
		String[] url=new String[1000];
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("Mehul.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			int rowStart = sheet.getFirstRowNum();
			int rowEnd = sheet.getLastRowNum();
			for (int i = rowStart + 1; i <= rowEnd; i++) {
				Row row = sheet.getRow(i);
				url[i] = row.getCell(0).getStringCellValue();
			}
			file.close();
		}
		catch(Exception e) {
			System.out.println("Error in getting Url");
		}
		return url;
	}
	
	
	
	public String[] getCommand() {
		String[] command=new String[1000];
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("Mehul.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			int rowStart = sheet.getFirstRowNum();
			int rowEnd = sheet.getLastRowNum();
			for (int i = rowStart + 1; i <= rowEnd; i++) {
				Row row = sheet.getRow(i);
				command[i] = row.getCell(2).getStringCellValue();
			}
			file.close();
		}
		catch(Exception e) {
			System.out.println("Error in getting Command");
		}
		return command;
	}
	
	public String[] getStringVar(int j) {
		String[] var=new String[1000];
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("Mehul.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			int rowStart = sheet.getFirstRowNum();
			int rowEnd = sheet.getLastRowNum();
			for (int i = rowStart + 1; i <= rowEnd; i++) {
				Row row = sheet.getRow(i);
				var[i] = row.getCell(3+j).getStringCellValue();
			}
			file.close();
		}
		catch(Exception e) {
			System.out.println("Error in getting var(1-6)");
		}
		return var;
	}
	
	public int[] getIntVar(int j) {
		int[] var=new int[1000];
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			FileInputStream file = new FileInputStream(classLoader.getResource("Mehul.xlsx").getFile());
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheetAt(0);
			int rowStart = sheet.getFirstRowNum();
			int rowEnd = sheet.getLastRowNum();
			for (int i = rowStart + 1; i <= rowEnd; i++) {
				Row row = sheet.getRow(i);
				var[i] = (int)row.getCell(3+j).getNumericCellValue();
			}
			file.close();
		}
		catch(Exception e) {
			System.out.println("Error in getting var7 or var8");
		}
		return var;
	}

}
