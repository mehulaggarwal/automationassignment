package TestNg;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import Model.ResponseAndToken;
import UtilityClasses.HitUrl;
import UtilityClasses.ReadFromExcel;
import UtilityClasses.ResponseDetails;


public class Test {

	
	int rowStart;
	int rowEnd;
	String[] var1=new String[1000];
	String[] var2=new String[1000];
	String[] var3=new String[1000];
	String[] var4=new String[1000];
	String[] var5=new String[1000];
	String[] var6=new String[1000];
	int[] var7=new int[1000];
	int[] var8=new int[1000];
	String[] url=new String[1000];
	String key="smsplus";
	String[] command=new String[1000];
	int form=2;
	
	@BeforeClass
	public void beforeClass() {
		ReadFromExcel re=new ReadFromExcel();
		rowStart=re.getRowStart();
		rowEnd=re.getRowEnd();
		url=re.getUrl();
		command=re.getCommand();
		var1=re.getStringVar(1);
		var2=re.getStringVar(2);
		var3=re.getStringVar(3);
		var4=re.getStringVar(4);
		var5=re.getStringVar(5);
		var6=re.getStringVar(6);
		var7=re.getIntVar(7);
		var8=re.getIntVar(8);
	}
	
	@org.testng.annotations.Test(priority=1)
	public void verify_save_user_card() {
		ResponseAndToken rt=HitUrl.hitUrl(form, url[1], key, command[1], var1[1], var2[1], var3[1], var4[1], var5[1], var6[1], var7[1], var8[1]);
		Assert.assertEquals(rt.getResponse().getStatusCode(), 200);
	}
	
	@org.testng.annotations.Test(priority=2)
	public void verify_get_user_card() {
		ResponseAndToken rt=HitUrl.hitUrl(form, url[2], key, command[2], var1[2], var2[2], var3[2], var4[2], var5[2], var6[2], var7[2], var8[2]);
		Assert.assertEquals(rt.getResponse().getStatusCode(), 200);
	}
	
	@org.testng.annotations.Test(priority = 3)
	public void verify_delete_user_card() {
		ResponseAndToken rt=HitUrl.hitUrl(form, url[3], key, command[3], var1[3], var2[3], var3[3], var4[3], var5[3], var6[3], var7[3], var8[3]);
		Assert.assertEquals(rt.getResponse().getStatusCode(), 200);
	}
	
	@org.testng.annotations.Test(priority=4)
	public void get_user_card_that_does_not_exist() {
		ResponseAndToken rt=HitUrl.hitUrl(form, url[4], key, command[4], var1[4], var2[4], var3[4], var4[4], var5[4], var6[4], var7[4], var8[4]);
		String msg=ResponseDetails.getMessage(rt.getResponse());
		Assert.assertEquals(msg,"Card not found.");
	}
	
	@org.testng.annotations.Test(priority=5)
	public void verify_response_headers_of_save_user_card() {
		ResponseAndToken rt=HitUrl.hitUrl(form, url[1], key, command[1], var1[1], var2[1], var3[1], var4[1], var5[1], var6[1], var7[1], var8[1]);
		String contentType=ResponseDetails.getContentType(rt.getResponse());
		Assert.assertEquals(contentType,"text/html");
	}
	
	@org.testng.annotations.Test(priority = 6)
	public void delete_card_that_does_not_exist() {
		ResponseAndToken rt=HitUrl.hitUrl(form, url[5], key, command[5], var1[5], var2[5], var3[5], var4[5], var5[5], var6[5], var7[5], var8[5]);
		String msg=ResponseDetails.getMessage(rt.getResponse());
		Assert.assertEquals(msg,"Card not found.");
	}
	

}
